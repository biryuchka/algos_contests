#include <deque>
#include <iostream>

std::deque<int> first, last;

void Request(char c) {
  if (c == '-') {
    std::cout << first.front() << "\n";
    first.pop_front();
  } else {
    int num;
    std::cin >> num;
    if (c == '*') {
      last.push_front(num);
    } else {
      last.push_back(num);
    }
  }
  if (last.size() > first.size()) {
    first.push_back(last.front());
    last.pop_front();
  }
}

int main() {
  int n;
  std::cin >> n;
  for (int i = 0; i < n; i++) {
    char c;
    std::cin >> c;
    Request(c);
  }
}
