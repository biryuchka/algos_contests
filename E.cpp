#include <algorithm>
#include <iostream>
#include <string>

const int kIntMax = 1e9;

struct StackElement {
  int value = kIntMax;
  int min_element = kIntMax;
  StackElement* next = nullptr;
};

class Stack {
 private:
  StackElement* head_ = nullptr;
  int size_ = 0;

 public:
  Stack() = default;
  ~Stack() {
    while (head_ != nullptr) {
      StackElement* element = head_;
      head_ = head_->next;
      delete element;
    }
  }

  bool Empty() { return (head_ == nullptr); }

  void Push(int n) {
    size_++;
    auto* element = new StackElement;
    element->value = n;
    element->min_element = (Empty() ? n : std::min(n, Min()));
    element->next = nullptr;
    if (!Empty()) {
      element->next = head_;
    }
    head_ = element;
  }

  int Pop() {
    size_--;
    StackElement* element;
    int tmp = Top();
    element = head_;
    head_ = head_->next;
    delete element;
    return tmp;
  }

  int& Top() { return head_->value; }
  const int& Top() const { return head_->value; }

  int Size() const { return size_; }

  void Clear() {
    while (!Empty()) {
      Pop();
    }
    size_ = 0;
  }

  int Min() {
    if (!Empty()) {
      return head_->min_element;
    }
    return kIntMax;
  }
};

class Queue {
 private:
  Stack s1_, s2_;
  size_t size_ = 0;
  const std::string kAns = "ok";

 public:
  Queue() = default;
  bool Empty() const { return (size_ == 0); }

  std::string Enqueue(int n) {
    size_++;
    s1_.Push(n);
    return kAns;
  }

  void Balance() {
    if (s2_.Empty()) {
      while (!s1_.Empty()) {
        s2_.Push(s1_.Pop());
      }
    }
  }

  int Dequeue() {
    size_--;
    Balance();
    return s2_.Pop();
  }

  int Front() {
    Balance();
    return s2_.Top();
  }

  size_t Size() const { return size_; }

  std::string Clear() {
    size_ = 0;
    s1_.Clear();
    s2_.Clear();
    return kAns;
  }

  int Min() {
    Balance();
    return std::min(s2_.Min(), s1_.Min());
  }
};

void Request(Queue& q) {
  std::string cmd;
  std::cin >> cmd;
  if (cmd == "enqueue") {
    int n;
    std::cin >> n;
    std::cout << q.Enqueue(n);
  } else if (cmd == "size") {
    std::cout << q.Size();
  } else if (q.Empty() &&
    (cmd == "front" || cmd == "min" || cmd == "dequeue")) {
    std::cout << "error";
  } else if (cmd == "clear") {
    std::cout << q.Clear();
  } else if (cmd == "dequeue") {
    std::cout << q.Dequeue();
  } else if (cmd == "front") {
    std::cout << q.Front();
  } else {
    std::cout << q.Min();
  }
  std::cout << "\n";
}

int main() {
  int m;
  std::cin >> m;
  Queue q;
  for (int k = 0; k < m; k++) {
    Request(q);
  }
}
