#include <cmath>
#include <iomanip>
#include <iostream>

void Input(double* arr, int n) {
  for (int i = 0; i < n; i++) {
    std::cin >> arr[i];
  }
}

void PrefSum(double* pref, double* arr, int n) {
  pref[0] = log(arr[0]);
  for (int i = 1; i < n; i++) {
    pref[i] = pref[i - 1] + log(arr[i]);
  }
}

void Request(const double* pref) {
  int l, r;
  std::cin >> l >> r;
  double x = (l == 0 ? pref[r] : pref[r] - pref[l - 1]), y = 1.0 / (r - l + 1);
  long double res = exp(y * x);
  std::cout << std::setprecision(10) << std::fixed << res << "\n";
}

int main() {
  int n;
  std::cin >> n;
  double* arr = new double[n];
  double* pref = new double[n];
  Input(arr, n);
  PrefSum(pref, arr, n);
  int q;
  std::cin >> q;
  for (int i = 0; i < q; i++) {
    Request(pref);
  }
  delete[] arr;
  delete[] pref;
}
