#include <iostream>
#include <vector>

long long NumByte(long long x, int n) { return x >> (8 * (n - 1)) & 255; }

void LSDSort(std::vector<long long>& a) {
  int bytes = sizeof(long long);
  for (int byte = 1; byte <= bytes; byte++) {
    std::vector<long long> cnt(256, 0), ind_help(a.size(), 0);
    for (size_t i = 0; i < a.size(); i++) {
      cnt[NumByte(a[i], byte)]++;
    }
    for (size_t i = 1; i < cnt.size(); i++) {
      cnt[i] += cnt[i - 1];
    }
    for (int i = int(a.size()) - 1; i >= 0; i--) {
      int num_byte = NumByte(a[i], byte);
      ind_help[--cnt[num_byte]] = a[i];
    }
    for (size_t i = 0; i < a.size(); i++) {
      a[i] = ind_help[i];
    }
  }
}

int main() {
  int n;
  std::cin >> n;
  std::vector<long long> arr(n);
  for (int i = 0; i < n; i++) {
    std::cin >> arr[i];
  }
  LSDSort(arr);
  for (long long& i : arr) {
    std::cout << i << "\n";
  }
}
