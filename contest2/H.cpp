#include <iostream>
#include <vector>

int Partition(std::vector<int>& a, int left_bound, int right_bound) {
  if (right_bound <= left_bound) {
    return left_bound;
  }
  int pivot = a[(left_bound + right_bound) / 2];
  int i = left_bound;
  int j = right_bound;
  while (i <= j) {
    while (a[i] < pivot) {
      i++;
    }
    while (a[j] > pivot) {
      j--;
    }
    if (i >= j) {
      break;
    }
    std::swap(a[i], a[j]);
    i++;
    j--;
  }
  return j;
}

void QuickSort(std::vector<int>& a, int left_bound, int right_bound) {
  if (left_bound < right_bound) {
    int mid = Partition(a, left_bound, right_bound);
    QuickSort(a, left_bound, mid);
    QuickSort(a, mid + 1, right_bound);
  }
}

void QuickSort(std::vector<int>& a) { QuickSort(a, 0, a.size() - 1); }

int main() {
  int n;
  std::cin >> n;
  std::vector<int> v(n);
  for (int i = 0; i < n; i++) {
    std::cin >> v[i];
  }
  QuickSort(v);
  for (int i = 0; i < n; i++) {
    std::cout << v[i] << " ";
  }
}
