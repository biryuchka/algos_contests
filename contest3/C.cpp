#include <iostream>
#include <vector>

class SegmentTree {
 private:
  std::vector<int> tree_;

 public:
  SegmentTree() = default;
  SegmentTree(int size) { tree_.resize(4 * size, 0); }

  void Update(int v, int tl, int tr, int pos, int val) {
    if (tl == tr) {
      tree_[v] = val;
    } else {
      int tm = (tl + tr) / 2;
      if (pos <= tm) {
        Update(v * 2, tl, tm, pos, val);
      } else {
        Update(v * 2 + 1, tm + 1, tr, pos, val);
      }
      tree_[v] = std::max(tree_[v * 2], tree_[v * 2 + 1]);
    }
  }

  int Request(int v, int tl, int tr, int pos, int val) {
    if (tree_[v] < val || tr < pos) {
      return -1;
    }
    if (tl == tr) {
      return tl + 1;
    }
    int tm = (tl + tr) / 2;
    int left = Request(v * 2, tl, tm, pos, val);
    if (left != -1) {
      return left;
    }
    return Request(v * 2 + 1, tm + 1, tr, pos, val);
  }

  ~SegmentTree() = default;
};

int main() {
  std::ios_base::sync_with_stdio(false);
  std::cin.tie(nullptr);
  int n, q;
  std::cin >> n >> q;
  SegmentTree tr(n);
  std::vector<int> arr(n);
  for (int i = 0; i < n; i++) {
    std::cin >> arr[i];
    tr.Update(1, 0, n - 1, i, arr[i]);
  }
  for (int k = 0; k < q; k++) {
    int cmd, i, x;
    std::cin >> cmd >> i >> x;
    if (cmd == 0) {
      tr.Update(1, 0, n - 1, i - 1, x);
    } else {
      std::cout << tr.Request(1, 0, n - 1, i - 1, x) << "\n";
    }
  }
}
