#include <algorithm>
#include <climits>
#include <iostream>
#include <vector>

using namespace std;

pair<pair<int, int>, pair<int, int>> MinPair(
    pair<pair<int, int>, pair<int, int>> p1,
    pair<pair<int, int>, pair<int, int>> p2) {
  vector<pair<int, int>> arr = {p1.first, p1.second, p2.first, p2.second};
  sort(arr.begin(), arr.end());
  return {arr[0], arr[1]};
}

vector<vector<pair<pair<int, int>, pair<int, int>>>> BuildSparse(
    const vector<int>& arr, const vector<int>& lgn) {
  int n = arr.size();
  int logn = lgn[n] + 1;
  vector<vector<pair<pair<int, int>, pair<int, int>>>> mn(
      logn, vector<pair<pair<int, int>, pair<int, int>>>(
                n, {{INT_MAX, INT_MAX}, {INT_MAX, INT_MAX}}));
  for (int i = 0; i < n; i++) {
    mn[0][i] = {{arr[i], i}, {arr[i], i}};
  }
  for (int i = 0; i < n; i++) {
    pair<int, int> p1 = {arr[i], i};
    if (i != n - 1) {
      pair<int, int> p2 = {arr[i + 1], i + 1};
      mn[1][i] = {min(p1, p2), max(p1, p2)};
    } else {
      mn[1][i] = {p1, p1};
    }
  }
  for (int i = 1; i < logn - 1; i++) {
    for (int j = 0; j < n; j++) {
      int k = min(n - 1, j + (1 << i));
      mn[i + 1][j] = MinPair(mn[i][j], mn[i][k]);
    }
  }
  return mn;
}

int main() {
  int n, q;
  cin >> n >> q;
  vector<int> arr(n);
  for (int i = 0; i < n; i++) {
    cin >> arr[i];
  }
  vector<int> lgn(n + 1, 0);
  for (int i = 2; i <= n; i++) {
    lgn[i] = lgn[i / 2] + 1;
  }
  vector<vector<pair<pair<int, int>, pair<int, int>>>> sparse =
      BuildSparse(arr, lgn);
  int l, r;
  while (q != 0) {
    cin >> l >> r;
    l--;
    r--;
    int k = lgn[r - l + 1];
    pair<pair<int, int>, pair<int, int>> p1, p2;
    p1 = sparse[k][l];
    p2 = sparse[k][r - (1 << k) + 1];
    vector<pair<int, int>> tmp = {p1.first, p1.second, p2.first, p2.second};
    sort(tmp.begin(), tmp.end());
    if (tmp[0] != tmp[1]) {
      cout << tmp[1].first << "\n";
    } else {
      cout << tmp[2].first << "\n";
    }
    q--;
  }
}
