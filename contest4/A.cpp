#include <algorithm>
#include <iostream>
#include <list>

class HashTable {
 private:
  int capacity_;
  int size_ = 0;
  const double kLoadFactor = 0.75;
  std::list<int>* table_ = nullptr;

  int Hash(int key) const { return (key % capacity_); }

  static bool Prime(int n) {
    if (n == 1) {
      return false;
    }
    for (int i = 2; i * i <= n; i++) {
      if (n % i == 0) {
        return false;
      }
    }
    return true;
  }

  void Swap(HashTable t1, HashTable t2) {
    std::swap(t1.table_, t2.table_);
    std::swap(t1.capacity_, t2.capacity_);
    std::swap(t1.size_, t2.size_);
  }

  void Rehash() {
    double load_factor = 1.0 * size_ / capacity_;
    if (load_factor > kLoadFactor) {
      HashTable tmp(2 * size_);
      for (int i = 0; i < capacity_; i++) {
        for (int& j : table_[i]) {
          tmp.Insert(j, j);
        }
      }
      Swap(tmp, *this);
    }
  }

 public:
  explicit HashTable(int size) {
    size += static_cast<int>(size % 2 == 0);
    while (!Prime(size)) {
      size += 2;
    }
    capacity_ = size;
    table_ = new std::list<int>[capacity_];
  }

  void Insert(int key, int data) {
    int index = Hash(key);
    if (!Find(key)) {
      table_[index].push_back(data);
      size_++;
      Rehash();
    }
  }

  void Erase(int key) {
    int index = Hash(key);
    for (auto i = table_[index].begin(); i != table_[index].end(); i++) {
      if (*i == key) {
        table_[index].erase(i);
        break;
      }
    }
  }

  bool Find(int key) {
    int index = Hash(key);
    for (int& i : table_[index]) {
      if (i == key) {
        return true;
      }
    }
    return false;
  }

  ~HashTable() { delete[] table_; }
};

int main() {
  int q;
  std::cin >> q;
  HashTable hash_table(q);
  for (int i = 0; i < q; i++) {
    char cmd;
    int el;
    std::cin >> cmd >> el;
    if (cmd == '+') {
      hash_table.Insert(el, el);
    } else if (cmd == '-') {
      hash_table.Erase(el);
    } else {
      std::cout << (hash_table.Find(el) ? "YES" : "NO") << "\n";
    }
  }
  return 0;
}
