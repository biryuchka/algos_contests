#include <algorithm>
#include <climits>
#include <iostream>
#include <vector>

int main() {
  int n, len = 0;
  std::cin >> n;
  std::vector<long long> arr(n);
  for (int i = 0; i < n; i++) {
    std::cin >> arr[i];
  }
  std::vector<long long> dp(n + 1, -INT_MAX);
  dp[0] = INT_MAX;
  std::vector<int> pos(n, -1);
  std::vector<int> prev(n);
  for (int i = 0; i < n; i++) {
    int ind = int(dp.rend() - std::lower_bound(dp.rbegin(), dp.rend(), arr[i]));
    if (dp[ind - 1] >= arr[i] && arr[i] > dp[ind]) {
      dp[ind] = arr[i];
      pos[ind] = i;
      prev[i] = pos[ind - 1];
      len = std::max(len, ind);
    }
  }
  std::cout << len << "\n";
  std::vector<int> ans(len);
  int p = pos[len];
  while (p != -1) {
    ans[--len] = p + 1;
    p = prev[p];
  }
  for (auto a : ans) {
    std::cout << a << " ";
  }
}
