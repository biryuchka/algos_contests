#include <algorithm>
#include <iostream>
#include <vector>

int main() {
  int n, m;
  std::cin >> n >> m;
  std::vector<int> res(n), cost(n);
  for (auto& i : res) {
    std::cin >> i;
  }
  for (auto& i : cost) {
    std::cin >> i;
  }
  std::vector<std::vector<int>> dp(n + 1, std::vector<int>(m + 1, 0));
  std::pair<int, int> ind;
  int maxi = 0;
  for (int i = 1; i <= n; i++) {
    for (int x = 1; x <= m; x++) {
      dp[i][x] = dp[i - 1][x];
      if (x >= res[i - 1]) {
        dp[i][x] = std::max(dp[i][x], dp[i - 1][x - res[i - 1]] + cost[i - 1]);
      }
      if (i == n && dp[i][x] > maxi) {
        ind = {i, x};
        maxi = dp[i][x];
      }
    }
  }
  std::vector<int> ans;
  int k = ind.first, s = ind.second;
  while (dp[k][s] != 0) {
    if (dp[k][s] != dp[k - 1][s]) {
      ans.push_back(k);
      s -= res[k - 1];
    }
    k--;
  }
  reverse(ans.begin(), ans.end());
  for (auto el : ans) {
    std::cout << el << '\n';
  }
}
