#include <algorithm>
#include <iostream>
#include <vector>

class Components {
 private:
  int n_;
  int num_comp_ = 1;
  std::vector<std::vector<int>> gr_, grp_;
  std::vector<bool> used_;
  std::vector<int> ord_, comp_;

  void Dfs1(int v) {
    used_[v] = true;
    for (auto to : gr_[v]) {
      if (!used_[to]) {
        Dfs1(to);
      }
    }
    ord_.push_back(v);
  }

  void Dfs2(int v) {
    comp_[v] = num_comp_;
    for (auto to : grp_[v]) {
      if (comp_[to] == 0) {
        Dfs2(to);
      }
    }
  }

  void Kosaraju() {
    for (int i = 0; i < n_; i++) {
      if (!used_[i]) {
        Dfs1(i);
      }
    }
    for (int i = n_ - 1; i >= 0; i--) {
      int v = ord_[i];
      if (comp_[v] == 0) {
        Dfs2(v);
        ++num_comp_;
      }
    }
    --num_comp_;
  };

 public:
  Components(int n, std::vector<std::vector<int>>& gr,
             std::vector<std::vector<int>>& grp)
      : n_(n), gr_(gr), grp_(grp) {
    comp_.resize(n, 0);
    used_.resize(n, false);
  }

  std::pair<int, std::vector<int>> Answer() {
    Kosaraju();
    return {num_comp_, comp_};
  }
};

int main() {
  int n, m;
  std::cin >> n >> m;
  std::vector<std::vector<int>> gr, grp;
  gr.resize(n);
  grp.resize(n);
  for (int i = 0; i < m; i++) {
    int x, y;
    std::cin >> x >> y;
    gr[--x].push_back(--y);
    grp[y].push_back(x);
  }
  Components solver(n, gr, grp);
  auto answer = solver.Answer();
  std::cout << answer.first << "\n";
  for (auto el : answer.second) {
    std::cout << el << " ";
  }
}
