#include <algorithm>
#include <climits>
#include <iostream>
#include <queue>
#include <vector>

struct Edge {
  int to;
  int cost;
};

std::vector<int> Dijkstra(int vertex_n, int start,
                          std::vector<std::vector<Edge>> graph) {
  std::vector<int> dist(vertex_n, INT_MAX);
  dist[start] = 0;
  std::priority_queue<std::pair<int, int>, std::vector<std::pair<int, int>>,
                      std::greater<std::pair<int, int>>>
      edges;
  edges.push(std::make_pair(0, start));
  while (!edges.empty()) {
    int cur_dist = edges.top().first;
    int vert = edges.top().second;
    edges.pop();
    if (cur_dist <= dist[vert]) {
      for (auto edge : graph[vert]) {
        int to = edge.to;
        int cost = edge.cost;
        if (dist[vert] + cost < dist[to]) {
          dist[to] = dist[vert] + cost;
          edges.push(std::make_pair(dist[to], to));
        }
      }
    }
  }
  return dist;
}

void Enter(int& vertex_n, int& edges_m, int& start,
           std::vector<std::vector<Edge>>& graph) {
  std::cin >> vertex_n >> edges_m;
  graph.resize(vertex_n);
  for (int i = 0; i < edges_m; i++) {
    int vert1;
    int vert2;
    int cost;
    std::cin >> vert1 >> vert2 >> cost;
    graph[vert1].push_back({vert2, cost});
    graph[vert2].push_back({vert1, cost});
  }
  std::cin >> start;
}

const int kAns = 2009000999;

int main() {
  int num_k;
  std::cin >> num_k;
  for (int k = 0; k < num_k; k++) {
    int vertex_n;
    int edges_m;
    int start;
    std::vector<std::vector<Edge>> graph;
    Enter(vertex_n, edges_m, start, graph);
    std::vector<int> dist = Dijkstra(vertex_n, start, graph);
    for (auto dist_i : dist) {
      std::cout << (dist_i != INT_MAX ? dist_i : kAns) << " ";
    }
    std::cout << "\n";
  }
}
