#include <algorithm>
#include <climits>
#include <iostream>
#include <vector>

struct Edge {
  int vert1;
  int vert2;
  long long cost;
};

const long long kCost = 100000;

int FordBellman(std::vector<Edge>& edges, std::vector<long long>& dist,
                std::vector<int>& parents, int fi, int vertex_n) {
  dist[fi] = 0;
  int flag;
  for (int i = 0; i < vertex_n; i++) {
    flag = -1;
    for (auto tmp : edges) {
      if (dist[tmp.vert1] < LONG_LONG_MAX) {
        if (dist[tmp.vert2] > dist[tmp.vert1] + tmp.cost) {
          dist[tmp.vert2] =
              std::max(-LONG_LONG_MAX, dist[tmp.vert1] + tmp.cost);
          parents[tmp.vert2] = tmp.vert1;
          flag = tmp.vert2;
        }
      }
    }
  }
  return flag;
}

std::vector<int> Path(int flag, int vertex_n, std::vector<int>& parents) {
  int start = flag;
  for (int i = 0; i < vertex_n; i++) {
    start = parents[start];
  }
  std::vector<int> cycle;
  int vert = start;
  do {
    cycle.push_back(vert + 1);
    vert = parents[vert];
  } while (vert != start);
  cycle.push_back(vert + 1);
  std::reverse(cycle.begin(), cycle.end());
  return cycle;
}

bool Solve(std::vector<Edge>& edges, std::vector<long long>& dist,
           std::vector<int>& parents, int fi, int vertex_n) {
  int flag = FordBellman(edges, dist, parents, fi, vertex_n);
  if (flag == -1) {
    if (fi == vertex_n - 1) {
      std::cout << "NO";
      return true;
    }
    return false;
  }
  std::cout << "YES\n";
  std::vector<int> cycle = Path(flag, vertex_n, parents);
  std::cout << cycle.size() << "\n";
  for (auto vert : cycle) {
    std::cout << vert << " ";
  }
  return true;
}

int main() {
  int vertex_n;
  std::cin >> vertex_n;
  std::vector<Edge> edges;
  for (int vert1 = 0; vert1 < vertex_n; vert1++) {
    for (int vert2 = 0; vert2 < vertex_n; vert2++) {
      long long cost;
      std::cin >> cost;
      if (cost != kCost) {
        edges.push_back({vert1, vert2, cost});
      }
    }
  }
  std::vector<long long> dist(vertex_n, LONG_LONG_MAX);
  std::vector<int> parents(vertex_n, -1);
  for (int fi = 0; fi < vertex_n; fi++) {
    if (Solve(edges, dist, parents, fi, vertex_n)) {
      return 0;
    }
  }
}
