#include <algorithm>
#include <iostream>
#include <vector>

struct Node {
  int v;
  int num_edge;
};

class Bridges {
  int n_, m_, cnt_ = 0, timer_ = 0;
  std::vector<std::vector<Node>> gr_;
  std::vector<bool> used_;
  std::vector<int> tin_, ret_;
  std::vector<bool> ans_;

  void Bridge(int num) {
    cnt_ += (!ans_[num]);
    ans_[num] = true;
  }

  void Dfs(int v, int p_edge = -1) {
    used_[v] = true;
    tin_[v] = ret_[v] = ++timer_;
    for (auto u : gr_[v]) {
      int to = u.v;
      int num = u.num_edge;
      if (num != p_edge) {
        if (used_[to]) {
          ret_[v] = std::min(ret_[v], tin_[to]);
        } else {
          Dfs(to, num);
          ret_[v] = std::min(ret_[v], ret_[to]);
          if (ret_[to] == tin_[to]) {
            Bridge(num);
          }
        }
      }
    }
  }

  void CallingDFS() {
    for (int i = 0; i < n_; i++) {
      if (!used_[i]) {
        Dfs(i);
      }
    }
  }
 public:
  Bridges(int n, int m, std::vector<std::vector<Node>>& gr)
    : n_(n), m_(m), gr_(gr){
    used_.resize(n);
    ans_.resize(m);
    ret_.resize(n);
    tin_.resize(n);
  }

  std::vector<int> BridgesAnswer() {
    CallingDFS();
    std::vector<int> res(cnt_);
    for (int i = 0, j = 0; i < m_ && j < cnt_; i++) {
      if (ans_[i]) {
        res[j] = i + 1;
        j++;
      }
    }
    return res;
  }
};

int main() {
  int n, m;
  std::cin >> n >> m;
  std::vector<std::vector<Node>> gr;
  gr.resize(n);
  for (int i = 0; i < m; i++) {
    int x, y;
    std::cin >> x >> y;
    gr[--x].push_back({--y, i});
    gr[y].push_back({x, i});
  }
  Bridges solver(n, m, gr);
  std::vector<int> ans = solver.BridgesAnswer();
  std::cout << ans.size() << "\n";
  for (auto ans_i : ans) {
    std::cout << ans_i << " ";
  }
}
