#include <algorithm>
#include <iostream>
#include <vector>

using namespace std;

class VectorCovering {
  int num_l_, num_n_;
  vector<int> match_;
  vector<bool> used_;
  vector<vector<int>> graph_;

  void dfs(int v, vector<bool>& used) {
    used[v] = true;
    for (auto to : graph_[v]) {
      if (!used[to]) {
        dfs(to, used);
      }
    }
  }

  vector<bool> dfs_calling() {
    vector<bool> used(num_n_, false);
    for (int i = 0; i < num_l_; ++i) {
      if (match_[i] < 0) {
        dfs(i, used);
      }
    }
    return used;
  }

 public:
  VectorCovering(int num_l, int num_r, vector<int>& match, vector<vector<int>>& graph)
  : num_l_(num_l), num_n_(num_l + num_r), match_(match) {
    graph_.resize(num_n_);
    for (int i = 0; i < num_l; ++i) {
      for (auto j : graph[i]) {
        if (j == match_[i]) {
          graph_[j + num_l].push_back(i);
        } else {
          graph_[i].push_back(j + num_l);
        }
      }
    }
    used_ = dfs_calling();;
  }

  vector<int> left() {
    vector <int> L_unused;
    for (int i = 0; i < num_n_; ++i) {
      if (i < num_l_ && !used_[i]) {
        L_unused.push_back(i + 1);
      }
    }
    return L_unused;
  }

  vector<int> right() {
    vector <int> R_used;
    for (int i = 0; i < num_n_; ++i) {
      if (i >= num_l_ && used_[i]) {
        R_used.push_back(i - num_l_ + 1);
      }
    }
    return R_used;
  }
};

void print_vec(vector<int>& vec) {
  cout << "\n" << vec.size();
  for (auto v : vec) {
    cout << " " << v;
  }
}

int main() {
  int num_l, num_r;
  cin >> num_l >> num_r;
  vector<vector<int>> graph(num_l + 1);
  for (int i = 0; i < num_l; ++i) {
    int vert, v;
    cin >> vert;
    while (vert--) {
      cin >> v;
      graph[i].push_back(--v);
    }
  }
  vector<int> match(num_l);
  for (int i = 0; i < num_l; ++i) {
    cin >> match[i];
    --match[i];
  }
  VectorCovering solver(num_l, num_r, match, graph);
  vector<int> L_unused = solver.left(), R_used = solver.right();
  cout << L_unused.size() + R_used.size();
  print_vec(L_unused);
  print_vec(R_used);
}
